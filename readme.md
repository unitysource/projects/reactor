# Reactor

![Game Logo](/images/logo.png)

## Table of Contents

- [About the Game](#about-the-game)
- [Gameplay](#gameplay)
- [Features](#features)
- [Getting Started](#getting-started)
- [Installation](#installation)
- [Controls](#controls)

## About the Game

Reactor is a science fiction miniature experimental nuclear reactor in which active particles move from wall to wall,
extracting energy

## Gameplay

At the first levels, the actors of the game are two spheres (particles) - elements charged with a certain energy that
move within a certain field of their presence. The laboratory assistant (player) placed two particles in a mini reactor
to develop a method for extracting energy as efficiently as possible. The player studies particles and influences their
behavior, changing the trajectory of these particles by participating in the process. To do this, the player has a
certain supply of passive particles and microdevices; depending on the level of difficulty and type of experiment, the
player’s arsenal will change. A laboratory assistant conducts an experiment. He is allocated a certain time and/or task
for this. That is, the conditions of the levels alternate. Sometimes a player must receive the maximum amount of energy
for some time, sometimes hold out as long as possible on a level, sometimes collect at least a certain amount of energy,
and these norms will increase from level to level, keeping the player in the corridor of interest.

![Game Screenshot 1](/images/screenshot1.jpg)
![Game Screenshot 2](/images/screenshot2.jpg)

## Features

* Changing the shape of the walls is the second type of experiment with the organization of space, both initially and
  during the experiment.
* The next type of experiment is mobile - this type is the most dynamic. In such modes, space transformations will be
  made and moving devices will be added: turntables, sliders, and the like.
* What follows is the most lively type of experiment, in which the game will be more reminiscent of pinball. There may
  be obstacles, both static and moving (such as carousels, turnstiles, traps, teleports, pulsers, particle dividers,
  labyrinths, etc. This is the most difficult, but also the most fun mode. Combinations of certain surprises will
  increase as the player progresses according to the plot.
* “BabyTime” -- A mode separated from the storyline - the results of this mode are not included in the statistics and
  are valid only until you exit this mode.

## Getting Started

To download the game, go to the [releases page]

### Installation

1. Clone the repository: `git clone https://gitlab.com/unitysource/projects/reactor.git`
2. Navigate to the game directory: `cd reactor`
3. Install dependencies: `npm install` or `yarn install`

### Controls

Only mobile controls are available at the moment.

**Mobile Controls:**

- Tap on the left side of the screen to move the left particle
- Tap on the right side of the screen to move the right particle
- Tap on the center of the screen to pause the game

## Acknowledgments

- [Sound effects by SoundBible](https://www.soundbible.com/)
